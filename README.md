# nanodet/nanodet-plus comments

#### 介绍
阅读 nanodet/nanodet-plus 时做的代码注释，干脆保存起来，以后方便自己复习。并不是每个源文件都有注释，这样没必要也浪费时间，只针对核心文件。

#### 使用说明

1.  2022.6.3 添加 atss_assigner.py 文件，搞明白了nanodet的正负样本选择。博客：https://blog.csdn.net/tangshopping/article/details/125105996 ；

2.  2022.6.10 添加 gfl_head.py, gfocal_loss.py , 搞明白 nanodet-loss 的调用以及 gfocal loss 的实现。博客：
https://blog.csdn.net/tangshopping/article/details/125223231 ；

3. 2022.11.23 添加 dsl_assigner.py 文件，搞明白了 nanodet-plus 的正负样本选择。博客：https://blog.csdn.net/tangshopping/article/details/128002292；





